package Animals;

public class Dog extends Animal {
    private int countLeg;

    public Dog(String name, int age, int countLeg) {
        super(name, age);
        this.countLeg = countLeg;
    }

    public Dog(String name, int age) {
        super(name, age);
        this.countLeg = 4;
    }


    public int getCountLeg() {
        return countLeg;
    }

    public void setCountLeg(int countLeg) {
        this.countLeg = countLeg;
    }

    @Override
    public void Say(){
        System.out.println("гав");
    }
}